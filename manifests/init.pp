# Class: pcm
# ===========================
#
# An example class for the Puppet Configuration Management course
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `parameter`
# Example parameter
#
# Examples
# --------
#
# @example
#    class { 'pcm':
#      parameter => 'value',
#    }
#
# Authors
# -------
#
# Toni Schmidbauer <toni@stderr.at>
#
# Copyright
# ---------
#
# Copyright 2017 Toni Schmidbauer
#
class pcm (
  String $parameter = 'value'
)
{

  notify { "This is the pcm example module in environment ${::environment}, pcm::parameter has the value ${::pcm::parameter}.": }

}
